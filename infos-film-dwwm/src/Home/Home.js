import { useState } from 'react';
import './Home.css';

function Home() {

    const [animation, setAnimation] = useState(true)

    const toggleAnimation = () => {
        console.log(animation)
        setAnimation(animation => !animation)
    }

    return (
        <div className="container">
            <div class="logo1">
                <img src={"../img/psyche.png"} className={`logo ${animation && "rotate"}`} alt="logo" />
                <img src={"../img/psyche.png"} className={`logo ${animation && "rotate"}`} alt="logo" />
                <img src={"../img/psyche.png"} className={`logo ${animation && "rotate"}`} alt="logo" />
            </div>
            <h1>TP React Développeur Web et Web Mobile</h1>
            <button
                className="App-button"
                onClick={toggleAnimation}>
                Modifier l’animation
            </button>
        </div >
    );
}

export default Home;