// import logo from './logo.svg';

// import { useState } from 'react';
import './App.css';
import { BrowserRouter as Router,Route, Routes } from 'react-router-dom';
import Home from './Home/Home';
import About from './About/About';
import Layout from './Layout/Layout';



function App() {
  // const [animation, setAnimation] = useState(true)

  // const toggleAnimation = () => {
  //   // console.log(animation)
  //   setAnimation(animation => !animation)
  // }

  return (
    <div className="App">

      <header className="App-header">
        {/* <img src="/img/psyche.png" className="App-logo" alt="logo" /> */}
        {/* <p>
          TP React Développeur Web et Web Mobile
        </p> */}
        {/* <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Rechercher des films
        </a> */}

        {/* <Home /> */}
 
      <Routes>
        <Route path="/" element={<Layout />} />
            <Route index element={<Home />} />         
            <Route path="/about" element={<About />} />
        {/* </Route>             */}
      </Routes>       

        {/* <button
        className="App-button"
        onClick={toggleAnimation}>
        Modifier l’animation
      </button> */}

      </header>

    </div>
  );
}

export default App;
